const express = require('express');
const router = express.Router();
const nodemailer = require('nodemailer');

require('dotenv').config();

const transport = {
  host: 'mail41.lwspanel.com',
  port: 465,
  secure: true,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASS,
  },
  tls: {
    rejectUnauthorized: false,
  },
};
const transporter = nodemailer.createTransport(transport);

router.post('/', (req, res) => {
  const name = req.body.name;
  const email = req.body.email || 'Pas demail';
  const subject = req.body.subject;
  const message = req.body.message;
  const content = `name: ${name} \n email: ${email} \n subject: ${subject} \n message: ${message} `;

  const mail = {
    from: name,
    to: 'contact@chambery-concerts.fr', //Change to email address that you want to receive messages on
    subject: `(Chambé Concerts) ${subject} `,
    text: content,
  };

  transporter.sendMail(mail, (err, data) => {
    try {
      res.json({
        msg: 'success',
      });
    } catch (error) {
      console.log(error);
      res.json({
        msg: 'fail',
      });
    }
  });
});

module.exports = router;
