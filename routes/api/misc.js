const express = require('express');
const { getArtistTopTrackUri } = require('../../services/spotifyService');
const router = express.Router();
const { generateTweets } = require('../api/twitter');
const fetch = require('node-fetch');

router.post('/force-tweet', async (req, res) => {
  let isFake = typeof req.body.isFake === 'boolean' ? req.body.isFake : true;
  const status = await generateTweets(isFake);
  return res.status(200).json(status);
});

router.post('/test-spotify', async (req, res) => {
  const test = await getArtistTopTrackUri(req.body.artistName);
  return res.status(200).json(test);
});

router.get('/force-deploy', async (req, res) => {
  fetch(
    'https://api.vercel.com/v1/integrations/deploy/QmUK9msGrcBxhHRkYp9mCm5nf12ysEpWwe2eNPYmoxH9C8/U81FJGxTWI'
  );
  return res.status(200).json('ca roule');
});

module.exports = router;
