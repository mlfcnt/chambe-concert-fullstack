const express = require('express');
const router = express.Router();
const moment = require('moment');

const Concert = require('../../models/Concert');
const { saveICToBdd } = require('../../services/infoConcertService');
const { saveBDZToBdd } = require('../../services/brinDeZincService');
const {
  saveJazzClubConcerts,
  saveJCToBdd,
} = require('../../services/jazzClubService');

// @route GET api/concerts
// @desc Get all concerts
// @access Public
router.get('/', (req, res) => {
  Concert.find()
    // classe par date ASC
    .sort({ date: 1 })
    .then((concerts) => res.json({ success: true, concerts }))
    .catch((err) => res.status(500).json(err));
});

// @route POST api/concerts
// @desc Get concerts for a given date
// @access Public
router.post('/date', (req, res) => {
  const date = req.body.date ? new Date(req.body.date) : new Date();
  Concert.find({
    startDate: {
      $gte: moment(date).startOf('day').toDate(),
      $lt: moment(date).endOf('day').toDate(),
    },
  })
    .sort({ date: 1 })
    .then((concerts) => res.json({ success: true, concerts }))
    .catch((err) => res.status(500).json(err));
});

router.post('/range', (req, res) => {
  const toDate = req.body.toDate;
  Concert.find({
    startDate: { $gte: new Date(), $lte: toDate },
  })
    .sort({ date: 1 })
    .then((concerts) => res.json({ success: true, concerts }))

    .catch((err) => res.status(500).json(err));
});

router.get('/force-refresh', (req, res) => {
  saveBDZToBdd();
  saveICToBdd();
  saveJCToBdd();
  return res.json('Oki doki');
});

// @route POST api/concerts
// @desc Create a concert
// @access Public
router.post('/', async (req, res) => {
  const newConcert = new Concert({
    startDate: req.body.startDate,
    artist: req.body.artist,
    description: req.body.description,
    price: req.body.price,
    isPartOfFestival: req.body.isPartOfFestival,
    festivalName: req.body.festivalName,
    venue: req.body.venue,
    urlDescription: req.body.urlDescription,
    largePicture: req.body.largePicture,
  });

  await newConcert
    .save()
    .then((result) => {
      res.status(200).json({
        success: true,
        newConcert: result,
      });
    })
    .catch((error) => res.status(500).json(error));
});

// @route POST api/concerts
// @desc Delete a concert (set DeletedAt to Date.now())
// @access Public
router.get('/delete/:id', async (req, res) => {
  await Concert.findByIdAndUpdate(req.params.id, {
    deletedAt: Date.now(),
  })
    .then((result) =>
      res.status(200).json({ success: true, editedConcert: result })
    )
    .catch((err) => res.status(501).json(err));
});

router.get('/jc', async (req, res) => {
  const concerts = await saveJazzClubConcerts();
  return res.status(200).json({ concerts });
});

router.delete('/', async (req, res) => {
  await Concert.deleteMany({ createdManually: false })
    .then(() => res.status(200).json({ success: true }))
    .catch((err) => res.status(501).json(err));
});

module.exports = router;
