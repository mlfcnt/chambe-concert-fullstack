const express = require('express');
const router = express.Router();
const Twitter = require('twitter');
const twitterService = require('../../services/twitterService');
const fs = require('fs');
const path = require('path');
const download = require('image-downloader');
const moment = require('moment');
const { Settings, DateTime } = require('luxon');
moment.locale('fr');
Settings.defaultLocale = 'fr';

require('dotenv').config();

const client = new Twitter({
  consumer_key: process.env.TWI_CONSUMER_KEY,
  consumer_secret: process.env.TWI_CONSUMER_SECRET,
  access_token_key: process.env.TWI_TOKEN_KEY,
  access_token_secret: process.env.TWI_TOKEN_SECRET,
});

exports.generateTweets = async (fake = false) => {
  try {
    const todayConcerts = await twitterService.getTodaysConcerts();
    if (todayConcerts.length === 0)
      return console.log('pas de concerts a tweeter');

    for (const concert of todayConcerts) {
      if (!concert.twitterHandle)
        concert.twitterHandle = await twitterService.getArtistTwitterHandle(
          client,
          concert
        );
      const options = {
        url:
          concert.largePicture ||
          concert.tumbnailPicture ||
          'https://images.unsplash.com/photo-1524368535928-5b5e00ddc76b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        dest: path.resolve(__dirname, './tweet.jpg'),
        extractFilename: false,
      };
      try {
        await download.image(options);
      } catch (e) {
        console.error('2', e);
      }
      const data = fs.readFileSync(path.resolve(__dirname, './tweet.jpg'));

      let status = {
        status: `🎵 Concert aujourd'hui à Chambéry (${DateTime.fromJSDate(
          concert.startDate
        ).toFormat('HH:mm')}) \n- Artiste : ${concert.artist.name}  ${
          concert.style ? `- (${concert.style})` : ''
        }  \n- Lieu : ${
          concert.venue.name
        }\n ➡ https://www.chambery-concerts.fr/\n #chambery #concert`,
      };
      if (fake === true) {
        return status;
      }
      client.post('media/upload', { media: data }, function (
        error,
        media,
        response
      ) {
        if (error) {
          console.error('1', error);
        }
        if (!error) {
          status.media_ids = media.media_id_string;
          status.handle = concert.handle;
          client.post('statuses/update', status, function (error, tweet) {
            console.log(2, status);
            if (!error) {
              console.log('tweet sent');
              fs.unlink(path.resolve(__dirname, './tweet.jpg'), function (err) {
                if (err) {
                  console.error(error);
                } else {
                  console.log('image supprimée');
                }
              });
            }
          });
          console.log(status);
        }
      });
    }
  } catch (error) {
    console.log('......err', error);
  }
};
