"use strict";

var express = require('express');

var router = express.Router();

var _require = require('node-schedule-tz'),
    scheduleJob = _require.scheduleJob;

var Twitter = require('twitter');

var twitterService = require('../../services/twitterService');

var fs = require('fs');

var path = require('path');

var download = require('image-downloader');

var moment = require('moment');

moment.locale('fr');

require('dotenv').config();

var client = new Twitter({
  consumer_key: process.env.TWI_CONSUMER_KEY,
  consumer_secret: process.env.TWI_CONSUMER_SECRET,
  access_token_key: process.env.TWI_TOKEN_KEY,
  access_token_secret: process.env.TWI_TOKEN_SECRET
}); // const job1 = () => {
//   schedule.scheduleJob('0 33 18 * * ?', async () => {
//     generateTweets();
//     console.log('job1');
//   });
// };

var jobAprem = function jobAprem() {
  scheduleJob('tweet apres midi', '30 15 * * *', 'Europe/Paris', function _callee() {
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            generateTweets();
            console.log('job2');

          case 2:
          case "end":
            return _context.stop();
        }
      }
    });
  });
};

var generateTweets = function generateTweets() {
  var todayConcerts, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _loop, _iterator, _step;

  return regeneratorRuntime.async(function generateTweets$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return regeneratorRuntime.awrap(twitterService.getTodaysConcerts());

        case 3:
          todayConcerts = _context3.sent;

          if (!(todayConcerts.length === 0)) {
            _context3.next = 7;
            break;
          }

          console.log('pas de concerts a tweeter'); // client.post(
          //   'statuses/update',
          //   {
          //     status: `${moment().format(
          //       'DD MMMM'
          //     )} : Il ne semble pas y avoir de concerts prévus pour auourd'hui à #Chambéry`,
          //   },
          //   (error, tweet, response) => {
          //     if (error) console.log('23', error);
          //   }
          // );

          return _context3.abrupt("return");

        case 7:
          _iteratorNormalCompletion = true;
          _didIteratorError = false;
          _iteratorError = undefined;
          _context3.prev = 10;

          _loop = function _loop() {
            var concert, options, data, status;
            return regeneratorRuntime.async(function _loop$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    concert = _step.value;
                    _context2.next = 3;
                    return regeneratorRuntime.awrap(twitterService.getArtistTwitterHandle(client, concert));

                  case 3:
                    concert.handle = _context2.sent;
                    options = {
                      url: concert.largePicture || // concert.tumbnailPicture ||
                      'https://images.unsplash.com/photo-1524368535928-5b5e00ddc76b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                      dest: path.resolve(__dirname, './tweet.jpg'),
                      extractFilename: false
                    };
                    _context2.prev = 5;
                    _context2.next = 8;
                    return regeneratorRuntime.awrap(download.image(options));

                  case 8:
                    _context2.next = 13;
                    break;

                  case 10:
                    _context2.prev = 10;
                    _context2.t0 = _context2["catch"](5);
                    console.error('2', _context2.t0);

                  case 13:
                    data = fs.readFileSync(path.resolve(__dirname, './tweet.jpg'));
                    status = {
                      status: "\uD83C\uDFB5 Concert aujourd'hui \xE0 Chamb\xE9ry (".concat(moment(concert.startDate).format('HH:mm'), ") \n- Artiste : ").concat(concert.handle, " \n- Lieu : ").concat(concert.venueTwitterHandle ? concert.venueTwitterHandle : concert.venue, "\n \u27A1 https://www.chambery-concerts.fr/concerts?").concat(moment().format('DD-MM-YYYY'), "\n #chambery #concert")
                    };
                    client.post('media/upload', {
                      media: data
                    }, function (error, media, response) {
                      if (error) {
                        console.error('1', error);
                      }

                      if (!error) {
                        status.media_ids = media.media_id_string;
                        status.handle = concert.handle;
                        client.post('statuses/update', status, function (error, tweet) {
                          if (!error) {
                            console.log('tweet sent');
                            fs.unlink(path.resolve(__dirname, './tweet.jpg'), function (err) {
                              if (err) {
                                console.error(error);
                              } else {
                                console.log('image supprimée');
                              }
                            });
                          }
                        });
                        console.log(status);
                      }
                    });

                  case 16:
                  case "end":
                    return _context2.stop();
                }
              }
            }, null, null, [[5, 10]]);
          };

          _iterator = todayConcerts[Symbol.iterator]();

        case 13:
          if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
            _context3.next = 19;
            break;
          }

          _context3.next = 16;
          return regeneratorRuntime.awrap(_loop());

        case 16:
          _iteratorNormalCompletion = true;
          _context3.next = 13;
          break;

        case 19:
          _context3.next = 25;
          break;

        case 21:
          _context3.prev = 21;
          _context3.t0 = _context3["catch"](10);
          _didIteratorError = true;
          _iteratorError = _context3.t0;

        case 25:
          _context3.prev = 25;
          _context3.prev = 26;

          if (!_iteratorNormalCompletion && _iterator["return"] != null) {
            _iterator["return"]();
          }

        case 28:
          _context3.prev = 28;

          if (!_didIteratorError) {
            _context3.next = 31;
            break;
          }

          throw _iteratorError;

        case 31:
          return _context3.finish(28);

        case 32:
          return _context3.finish(25);

        case 33:
          _context3.next = 38;
          break;

        case 35:
          _context3.prev = 35;
          _context3.t1 = _context3["catch"](0);
          console.log('......err', _context3.t1);

        case 38:
        case "end":
          return _context3.stop();
      }
    }
  }, null, null, [[0, 35], [10, 21, 25, 33], [26,, 28, 32]]);
};

jobAprem();
module.exports = router;