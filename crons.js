const { scheduleJob } = require('node-schedule-tz');
const { generateTweets } = require('./routes/api/twitter');
const { saveBDZToBdd } = require('./services/brinDeZincService');
const { saveICToBdd } = require('./services/infoConcertService');
const { saveJCToBdd } = require('./services/jazzClubService');
const fetch = require('node-fetch');

exports.tweetApresMidi = async () => {
  scheduleJob('tweet apres midi', '30 15 * * *', 'Europe/Paris', async () => {
    console.log("tweet de l'apres midi");
    await generateTweets();
  });
};

exports.saveConcertsEveryHour = async () => {
  console.log('inside save concerts');
  scheduleJob('save concerts', '0 0 */1 * *', 'Europe/Paris', async () => {
    try {
      const [concertsBDZ, concertsIC] = await Promise.all([
        saveBDZToBdd,
        saveICToBdd,
      ]);
      const newConcerts = concertsBDZ + concertsIC;
      if (!newConcerts) return;
      this.buildAndDeploy();
    } catch (error) {
      console.log('..................', error);
    }
  });
};

exports.buildAndDeploy = async () => {
  async () => {
    try {
      fetch(
        'https://api.vercel.com/v1/integrations/deploy/QmUK9msGrcBxhHRkYp9mCm5nf12ysEpWwe2eNPYmoxH9C8/U81FJGxTWI'
      );
    } catch (error) {
      console.log('deploying error', error);
    }
  };
};
