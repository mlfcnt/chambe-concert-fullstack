import {
  getConcertsFromBDDByDate,
  getNexConcertsFromBDDByDateRange,
} from '../Api';

export const loadConcerts = async (
  chosenDate,
  setLoadingConcerts,
  setLoadingError,
  setTodaysConcerts
) => {
  setLoadingConcerts(true);
  try {
    const { concerts } = await getConcertsFromBDDByDate(chosenDate);
    if (concerts === undefined) return setLoadingError(true);
    setTodaysConcerts(concerts);
    setLoadingConcerts(false);
  } catch (error) {
    return (
      !error.message.includes('aborted') &&
      (console.error(error), setLoadingError(error.message))
    );
  }
};
export const loadNextConcerts = async (setLoading, range) => {
  try {
    const { concerts } = await getNexConcertsFromBDDByDateRange(range);
    setLoading(false);
    return concerts;
  } catch (error) {
    return !error.message.includes('aborted') && console.error(error);
  }
};
