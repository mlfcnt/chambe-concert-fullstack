import React from 'react';
import { orange } from '../constants/couleurs';
import moment from 'moment';
import { capitalizeFirstLetters } from '../helper/capitalizeHelper';

export const NextShowsDate = ({ date }) => {
  return (
    <p
      style={{
        textAlign: 'center',
        fontSize: '1.5rem',
        color: orange,
        marginBottom: '3vh',
      }}
    >
      {capitalizeFirstLetters(moment(date).format('dddd D MMMM YYYY'))}
    </p>
  );
};
