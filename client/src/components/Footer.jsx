import React from 'react';
import TwitterIcon from '@material-ui/icons/Twitter';
import { makeStyles } from '@material-ui/core/styles';
import { ContactForm } from './ContactForm';
import FaceIcon from '@material-ui/icons/Face';
import LogoSimplon from '../simplon.png';
import { bleuFonce, grisClair } from '../constants/couleurs';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: bleuFonce,
  },
  paper: {
    backgroundColor: grisClair,
    minWidth: '50vw',
    border: '3px solid #273253',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    maxWidth: '80%',
  },
  footer: {
    marginTop: '4rem',
    backgroundColor: bleuFonce,
    display: 'flex',
    justifyContent: 'center',
    color: grisClair,
    alignItems: 'center',
  },
}));

export const Footer = () => {
  const classes = useStyles();

  return (
    <>
      <footer className={classes.footer}>
        <div className="footerBtn">
          <ContactForm />
        </div>
        <div className="footerBtn">
          <a
            href="https://twitter.com/chambe_concerts"
            target="_blank"
            rel="noopener noreferrer"
          >
            <TwitterIcon />
          </a>
        </div>
        <div className="footerBtn simplonBtn">
          <a
            href="https://auvergnerhonealpes.simplon.co/chambery.html"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src={LogoSimplon} alt="Simplon" />
          </a>
        </div>
      </footer>
    </>
  );
};
