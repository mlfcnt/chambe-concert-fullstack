import React from 'react';
import moment from 'moment';
import DatePicker from 'react-date-picker';
import EventIcon from '@material-ui/icons/Event';
import { eModes } from '../constants/modes';
import { DateTime } from 'luxon';

export const DateShortcutButtons = ({ chosenDate, history, setToMode }) => {
  const buttonPadding = '0.6vw';

  return (
    <div className="shortcutButtons" style={{ marginBottom: '3rem' }}>
      <DatePicker
        onChange={(date) => {
          setToMode(eModes.regular);
          return history.push({
            pathname: '/concerts',
            search: `${moment(date).format('DD-MM-YYYY')}`,
          });
        }}
        clearIcon={null}
        locale="fr"
        calendarIcon={<EventIcon />}
      />

      {moment(chosenDate).format('MM/D/YYYY') !==
        moment().format('MM/D/YYYY') && (
        <button
          onClick={() => {
            setToMode(eModes.regular);
            history.push({
              pathname: '/concerts',
              search: `${moment().format('DD-MM-YYYY')}`,
            });
          }}
          style={{ padding: buttonPadding }}
        >
          Aujourd'hui
        </button>
      )}
      <button
        onClick={() => {
          setToMode(eModes.nextShows);
          history.push({
            pathname: '/prochains-concerts',
          });
        }}
        style={{ padding: buttonPadding }}
      >
        Prochains concerts
      </button>
    </div>
  );
};
