import React, { useState, useEffect } from 'react';
import { getSpotifBestTrackForArtist } from '../Api.js';
import { capitalizeFirstLetters } from '../helper/capitalizeHelper';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FaceIcon from '@material-ui/icons/Face';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import { Button } from '@material-ui/core';
import { orange, grisClair, bleuFonce } from '../constants/couleurs.js';
import { Settings, DateTime } from 'luxon';
Settings.defaultLocale = 'fr';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    marginBottom: '3rem',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  title: {
    fontSize: 24,
  },
  pos: {
    fontSize: 30,
    marginBottom: 12,
  },
});

export const ConcertCard = ({ concertData, cardSize }) => {
  const {
    // _id,
    artist,
    style,
    venue,
    startDate,
    description,
    price,
    urlDescription,
    largePicture,
    tumbnailPicture,
    isCanceled,
  } = concertData;
  const [uri, setUri] = useState();
  const classes = useStyles();
  const maxDescriptionLength = 500;

  useEffect(() => {
    if (!artist) return;
    (async () => {
      const uri = await getSpotifBestTrackForArtist(artist.name);
      if (!uri) {
        return setUri('error');
      }
      const type = uri.split(':')[1];
      const actualUri = uri.split(':')[2];
      setUri({ type, actualUri });
    })();
  }, [artist]);

  const finalCardSize = 400 + cardSize * 8 + 'px';
  return (
    <Card
      className={classes.root}
      variant="outlined"
      style={{ width: finalCardSize, borderRadius: 0 }}
    >
      {largePicture && (
        <CardMedia
          className={classes.media}
          image={largePicture || tumbnailPicture}
          title={`Photo de ${artist?.name || "l'artiste"}`}
        />
      )}
      <CardContent>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {DateTime.fromISO(startDate).toLocaleString(DateTime.TIME_24_SIMPLE)}
          {isCanceled && (
            <span>
              - <span style={{ color: '#ed5c4d' }}> Annulé</span>{' '}
            </span>
          )}
        </Typography>
        <Typography variant="h3" component="h2">
          {capitalizeFirstLetters(artist?.name || '?')}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          {capitalizeFirstLetters(venue?.name || '?')}{' '}
          {price ? '- ' + price : ''}
        </Typography>
        {style && (
          <Typography
            variant="body2"
            component="p"
            style={{
              color: bleuFonce,
              marginBottom: '1vh',
              fontSize: '1.2rem',
            }}
          >
            <strong>{style}</strong>
          </Typography>
        )}

        {description && (
          <Typography
            variant="body2"
            component="p"
            style={{ fontSize: '1rem' }}
          >
            {description.length > maxDescriptionLength
              ? description.substring(0, maxDescriptionLength) + ' [...]'
              : description}
            <br />
          </Typography>
        )}
        {urlDescription && (
          <div style={{ textAlign: 'center' }}>
            <Button
              variant="contained"
              style={{
                color: grisClair,
                backgroundColor: orange,
                borderRadius: 0,
                marginTop: '2vh',
              }}
              href={urlDescription}
              target="_blank"
            >
              + Info / résa
            </Button>
          </div>
        )}
        {uri && uri !== 'error' && (
          <div style={{ textAlign: 'center' }}>
            <iframe
              src={`https://open.spotify.com/embed/${uri.type}/${uri.actualUri}`}
              width="300"
              height="80"
              frameBorder="0"
              allowtransparency="true"
              allow="encrypted-media"
              style={{ marginTop: '2rem' }}
              title="Spotify widget"
            ></iframe>
          </div>
        )}
      </CardContent>
      <CardActions>
        {artist && artist.type === 'Band' && (
          <IconButton aria-label="icon groupe">
            <SupervisorAccountIcon />
          </IconButton>
        )}
        {artist && artist.type === 'Solo' && (
          <IconButton aria-label="icon artiste solo">
            <FaceIcon />
          </IconButton>
        )}
      </CardActions>
    </Card>
  );
};
