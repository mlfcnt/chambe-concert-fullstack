import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import MailOutlineSharpIcon from '@material-ui/icons/MailOutlineSharp';
import { Formik } from 'formik';
import { sendEmail } from '../Api.js';
import { toast } from 'react-toastify';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import { bleuFonce, grisClair, orange } from '../constants/couleurs.js';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: bleuFonce,
  },
  paper: {
    backgroundColor: grisClair,
    minWidth: '50vw',
    border: '3px solid #273253',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    maxWidth: '80%',
  },
  field: {
    borderRadius: 0,
  },
}));

export const ContactForm = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const marginBetweenInputs = '2vh';

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const customHandleSubmit = async ({ name, email, subject, message }) => {
    let toastMail;
    toastMail = toast.info("Envoi de l'email...", { autoClose: false });

    const { msg } = await sendEmail({
      name,
      email: email ?? '?',
      subject,
      message,
    });
    if (msg === 'success') {
      setOpen(false);
      return toast.update(toastMail, {
        render: 'Mail envoyé avec succès',
        type: toast.TYPE.SUCCESS,
        autoClose: 5000,
      });
    }
    setOpen(false);
    return toast.update(toastMail, {
      render: "Erreur lors de l'envoi de l'email :( Veuillez réessayer",
      type: toast.TYPE.ERROR,
    });
  };
  return (
    <div className="contactForm">
      <p className="contactBtn" type="button" onClick={handleOpen}>
        <span>
          <MailOutlineSharpIcon />
        </span>
      </p>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <Formik
              initialValues={{ name: '', email: '', subject: '', message: '' }}
              onSubmit={(values, { setSubmitting }) => {
                setSubmitting(false);
                customHandleSubmit(values);
              }}
            >
              {({ values, handleChange, handleSubmit, isSubmitting }) => (
                <form
                  onSubmit={handleSubmit}
                  className="TextField-without-border-radius"
                >
                  <p style={{ marginBottom: marginBetweenInputs }}>
                    Questions, remarques, suggestions,... contactez-nous :
                  </p>
                  <TextField
                    id="standard-read-only-input"
                    name="name"
                    label="Nom"
                    onChange={handleChange}
                    variant="outlined"
                    value={values.name}
                    style={{ marginBottom: marginBetweenInputs }}
                    size={'small'}
                    required
                    fullWidth
                  />

                  <br />
                  <TextField
                    id="email-input"
                    type="email"
                    name="email"
                    label="Email"
                    onChange={handleChange}
                    variant="outlined"
                    value={values.email}
                    style={{
                      marginBottom: marginBetweenInputs,
                    }}
                    helperText="À remplir si vous souhaitez être recontacté"
                    fullWidth
                    size={'small'}
                  />
                  <br />
                  <TextField
                    id="subject-input"
                    name="subject"
                    label="Sujet"
                    onChange={handleChange}
                    variant="outlined"
                    value={values.subject}
                    style={{ marginBottom: marginBetweenInputs }}
                    required
                    fullWidth
                  />
                  <br />
                  <TextField
                    id="multiline-message-input"
                    name="message"
                    label="Message"
                    onChange={handleChange}
                    variant="outlined"
                    value={values.message}
                    style={{ marginBottom: marginBetweenInputs }}
                    multiline
                    rows={3}
                    fullWidth
                    required
                  />
                  <br />
                  <Button
                    variant="contained"
                    type="submit"
                    style={{
                      color: grisClair,
                      backgroundColor: orange,
                      borderRadius: 0,
                      marginBottom: marginBetweenInputs,
                    }}
                    disabled={isSubmitting}
                  >
                    Envoyer
                  </Button>
                  <p>contact@chambery-concerts.fr</p>
                </form>
              )}
            </Formik>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};
