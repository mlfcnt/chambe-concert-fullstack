import React from 'react';
import { ConcertCard } from './ConcertCard';
import moment from 'moment';
import Typography from '@material-ui/core/Typography';

export const DisplayTodaysConcerts = ({
  loadingError,
  todaysConcerts,
  chosenDate,
  cardSize,
}) => {
  return !loadingError && todaysConcerts && !todaysConcerts.length ? (
    <div>
      <Typography
        style={{
          fontSize: '1.4rem',
          display: 'block',
          marginBottom: '2vw',
          textAlign: 'center',
        }}
      >
        {moment(chosenDate).format('DD/MM/YYYY') ===
        moment().format('DD/MM/YYYY')
          ? "Pas de concerts trouvés pour aujourd'hui"
          : 'Pas de concerts trouvés à cette date'}
      </Typography>
    </div>
  ) : (
    <div className="concertCardWrapper">
      {todaysConcerts
        .sort(
          (a, b) =>
            moment(a.startDate).format('HHmm') -
            moment(b.startDate).format('HHmm')
        )
        .map((concert) => (
          <ConcertCard
            key={concert._id}
            concertData={concert}
            cardSize={cardSize}
          />
        ))}
    </div>
  );
};
