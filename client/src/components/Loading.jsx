import React, { useState, useEffect } from 'react';

/**
 * Affiche un loader
 */
export const Loading = () => {
  const [loadingText, setLoadingText] = useState('');

  useEffect(() => {
    let baseText = '';
    const timer = setInterval(() => {
      baseText = `${baseText}.`;
      if (baseText.length > 8) {
        baseText = '.';
      }
      setLoadingText(baseText);
    }, 300);
    return () => clearInterval(timer);
  }, []);

  return <p className="loadingDots">{loadingText}</p>;
};
