import React from 'react';
import { IncrementNumber } from './IncrementNumber';

export const MainDate = ({ chosenDate, history }) => {
  const iconSize = 40;
  const spaceBetween = '5vw';
  const dateSize = '5vw';
  const arrowPadding = 0;
  return (
    <div
      className={'mainDate'}
      style={{
        display: 'flex',
        justifyContent: 'center',
        marginTop: '1.5rem',
      }}
    >
      <div style={{ marginRight: spaceBetween, marginBottom: '1rem' }}>
        <IncrementNumber
          chosenDate={chosenDate}
          history={history}
          type={'d'}
          typeOfFormat={'ddd'}
          iconSize={iconSize}
          displayButtons={false}
          dateSize={dateSize}
          padding={arrowPadding}
        />
      </div>
      <div style={{ marginRight: spaceBetween }}>
        <IncrementNumber
          chosenDate={chosenDate}
          history={history}
          type={'d'}
          typeOfFormat={'DD'}
          iconSize={iconSize}
          displayButtons
          dateSize={dateSize}
          padding={arrowPadding}
        />
      </div>
      <div style={{ marginRight: spaceBetween }}>
        <IncrementNumber
          chosenDate={chosenDate}
          history={history}
          type={'M'}
          typeOfFormat={'MM'}
          iconSize={iconSize}
          displayButtons
          dateSize={dateSize}
          padding={arrowPadding}
        />
      </div>
      <IncrementNumber
        chosenDate={chosenDate}
        history={history}
        type={'y'}
        typeOfFormat={'YYYY'}
        iconSize={iconSize}
        displayButtons
        dateSize={dateSize}
        padding={arrowPadding}
      />
    </div>
  );
};
