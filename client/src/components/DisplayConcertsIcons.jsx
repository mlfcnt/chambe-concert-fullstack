import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUsers, faUser } from '@fortawesome/free-solid-svg-icons';

/**
 * Affiche les icons donnant des infos sur le type de concert, l'artiste,...
 * @param {artist} artist
 */
export const DisplayConcertsIcons = ({ artist }) => {
  const iconsToDisplay = [];

  const soloArtistOrBandIcon = () => {
    if (artist && artist.type) {
      switch (artist.type) {
        case 'Band':
          iconsToDisplay.push(
            <span title={'Groupe'} key={artist.name || artist}>
              <FontAwesomeIcon icon={faUsers} />
            </span>
          );
          break;
        case 'Solo':
          iconsToDisplay.push(
            <span title={'Artiste solo'} key={artist.name || artist}>
              <FontAwesomeIcon icon={faUser} />
            </span>
          );
          break;
        default:
          return;
      }
    }
  };
  soloArtistOrBandIcon();
  return iconsToDisplay.map(i => i);
};
