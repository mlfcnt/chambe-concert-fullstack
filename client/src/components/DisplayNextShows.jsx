import React, { useState, useEffect } from 'react';
import { loadNextConcerts } from '../components/loadConcerts';
import { Loading } from '../components/Loading';
import moment from 'moment';
import { groupBy } from 'lodash';
import Button from '@material-ui/core/Button';
import { NextShowsCard } from './NextShowsCard';
import { NextShowsDate } from './NextShowsDate';
import { bleuFonce, grisClair } from '../constants/couleurs';

export const DisplayNextShows = () => {
  const [loading, setLoading] = useState(true);
  const [concerts, setConcerts] = useState([]);
  const [range, extendRange] = useState(2);

  useEffect(() => {
    setLoading(true);
    const toDate = moment().add(range, 'months');
    (async () => {
      const concerts = await loadNextConcerts(setLoading, toDate);
      setConcerts(concerts);
    })();
  }, [range]);

  if (!concerts.length && loading) return <Loading />;

  const displayConcerts = () => {
    const grouppedByDate = groupBy(concerts, ({ startDate }) => {
      return moment(startDate).startOf('day');
    });
    const dates = [];
    for (const date in grouppedByDate) {
      dates.push(moment(date));
    }
    return (
      <>
        {dates
          .sort((a, b) => moment(a) - moment(b))
          .map((d) => (
            <div style={{ margin: '50px' }}>
              <NextShowsDate date={d} />
              {grouppedByDate[d].map((concert) => {
                return (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-around',
                      marginBottom: '2vw',
                    }}
                  >
                    <NextShowsCard concert={concert} />
                  </div>
                );
              })}
            </div>
          ))}
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Button
            variant="contained"
            style={{ backgroundColor: bleuFonce, color: grisClair }}
            onClick={() => extendRange(range + 2)}
          >
            {concerts.length && loading ? 'Chargement...' : ' Afficher plus'}
          </Button>
        </div>
      </>
    );
  };

  return displayConcerts();
};
