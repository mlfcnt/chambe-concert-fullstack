import React from 'react';
import moment from 'moment';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { IconButton } from '@material-ui/core';
import { capitalizeFirstLetter } from '../helper/capitalizeHelper';
require('moment/locale/fr');

export const IncrementNumber = ({
  type,
  typeOfFormat,
  chosenDate,
  iconSize,
  displayButtons,
  dateSize,
  padding,
  history,
}) => {
  const switchNumber = (operation) => {
    return operation === 'add'
      ? history.push({
          pathname: '/concerts',
          search: `${moment(chosenDate).add(1, type).format('DD-MM-YYYY')}`,
        })
      : history.push({
          pathname: '/concerts',
          search: `${moment(chosenDate)
            .subtract(1, type)
            .format('DD-MM-YYYY')}`,
        });
  };

  const getWidth = () => {
    switch (typeOfFormat) {
      case 'dddd':
        return '19vw';
      case 'ddd':
        return '8vw';
      case 'DD':
        return '5vw';
      case 'MMMM':
        return '20vw';
      case 'MM':
        return '5vw';
      case 'YYYY':
        return '9vw';

      default:
        return '18vw';
    }
  };

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        fontSize: '3vw',
        textAlign: 'center',
        width: getWidth(),
      }}
    >
      <IncrementButton
        switchNumber={switchNumber}
        displayButtons={displayButtons}
        iconSize={iconSize}
        padding={padding}
      />
      <ActualDate
        dateSize={dateSize}
        chosenDate={chosenDate}
        typeOfFormat={typeOfFormat}
      />
      <DecrementButton
        switchNumber={switchNumber}
        displayButtons={displayButtons}
        iconSize={iconSize}
        type={type}
        chosenDate={chosenDate}
        padding={padding}
      />
    </div>
  );
};

export const IncrementButton = ({
  switchNumber,
  displayButtons,
  iconSize,
  padding,
}) => {
  return (
    <IconButton
      onClick={() => {
        switchNumber('add');
      }}
      style={{
        backgroundColor: 'transparent',
        color: '#0000004d',
        visibility: !displayButtons && 'hidden',
        padding: padding,
      }}
    >
      <ExpandLessIcon
        style={{ fontSize: iconSize }}
        className={'incrementBtn'}
      />
    </IconButton>
  );
};

export const DecrementButton = ({
  switchNumber,
  displayButtons,
  iconSize,
  type,
  chosenDate,
  padding,
}) => {
  return (
    <IconButton
      onClick={() => switchNumber('subtract')}
      style={{
        backgroundColor: 'transparent',
        color: '#0000004d',
        visibility: !displayButtons && 'hidden',
        padding: padding,
      }}
    >
      <ExpandMoreIcon
        style={{ fontSize: iconSize }}
        className={'incrementBtn'}
      />
    </IconButton>
  );
};

export const ActualDate = ({ dateSize, chosenDate, typeOfFormat }) => {
  return (
    <div style={{ width: '100%' }}>
      <span style={{ fontSize: dateSize }}>
        {capitalizeFirstLetter(moment(chosenDate).format(typeOfFormat))}
      </span>
    </div>
  );
};
