import React from 'react';
import moment from 'moment';

/**
 * Si l'heure passée en entrée correspond à l'heure actuelle, l'affiche en rouge
 * @param {Date} date - La date comprenant l'heure à afficher
 */
export const DisplayTime = ({ date, startHour }) => {
  //? commenté car il faut formatter jazz club et brin de zinc comme il faut
  // const isNow =
  //   moment().startOf('hour').format('HH:mm') ===
  //   moment(date).startOf('hour').format('HH:mm');
  return (
    // <span style={{ color: isNow ? 'red' : 'inherit' }}>
    <span>
      {startHour
        ? startHour.replace('h', ':')
        : moment(date).format('HH:mm').replace('h', ':')}
    </span>
  );
};
