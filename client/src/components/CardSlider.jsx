import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import VolumeDown from '@material-ui/icons/VolumeDown';
import VolumeUp from '@material-ui/icons/VolumeUp';
import { orange } from '../constants/couleurs';

const useStyles = makeStyles({
  root: {
    width: 200,
  },
});

export const CardSlider = ({ setCardSize }) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
    setCardSize(newValue);
  };

  return (
    <div className={classes.root}>
      {/* <Typography id="continuous-slider" gutterBottom>
        Taille des cartes
      </Typography> */}
      <Grid container spacing={2}>
        {/* <Grid item>
          <VolumeDown />
        </Grid> */}
        <Grid item xs>
          <Slider
            value={value}
            onChange={handleChange}
            aria-labelledby="continuous-slider"
            style={{ color: orange }}
            min={0}
            max={100}
            step={1}
            defaultValue={0}
          />
        </Grid>
        {/* <Grid item>
          <VolumeUp />
        </Grid> */}
      </Grid>
    </div>
  );
};
