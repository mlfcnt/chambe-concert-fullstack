import React, { useEffect, useState } from 'react';
import moment from 'moment';
import '../../src/App.css';
import { Loading } from './Loading';
import { DisplayTodaysConcerts } from './DisplayTodaysConcerts';
import { loadConcerts } from './loadConcerts';
import { useHistory, useLocation } from 'react-router-dom';
import { MainDate } from './MainDate';
import { DateShortcutButtons } from './DateShortcutButtons';
import { Footer } from './Footer';
import ReactGA from 'react-ga';
import { DisplayNextShows } from './DisplayNextShows';
import { toast } from 'react-toastify';
import { eModes } from '../constants/modes';
ReactGA.initialize('UA-164044205-1');
ReactGA.pageview(window.location.pathname + window.location.search);

console.log('%c Coucou', 'background: #222; color: #bada55');

/**
 * Page principale
 */
function Home() {
  const [todaysConcerts, setTodaysConcerts] = useState([]);
  const [loadingConcerts, setLoadingConcerts] = useState(true);
  const [loadingError, setLoadingError] = useState(false);
  const [chosenDate, setChosenDate] = useState();
  const [cardSize] = useState(0);
  const [mode, setToMode] = useState(eModes.regular);

  const history = useHistory();
  const { search } = useLocation();

  useEffect(() => {
    setLoadingError(false);
    if (
      !search ||
      moment(search.replace('?', '')).format() > moment().format()
    ) {
      setLoadingConcerts(true);
      return history.push({
        pathname: '/concerts',
        search: `${moment().format('DD-MM-YYYY')}`,
      });
    }
  }, []);

  useEffect(() => {
    if (!search) return;
    const currentSearch = search.replace('?', '').replace(/-/g, '/');
    const splittedDate = currentSearch.split('/');
    const actualDate = moment(
      `${splittedDate[0]}-${splittedDate[1]}-${splittedDate[2]} 12:00`,
      'DD-MM-YYYY HH:mm'
    );
    return setChosenDate(actualDate);
  }, [search]);

  useEffect(() => {
    if (!chosenDate) return;
    setLoadingConcerts(true);
    setToMode(eModes.regular);
    loadConcerts(
      chosenDate,
      setLoadingConcerts,
      setLoadingError,
      setTodaysConcerts,
      loadConcerts
    );
  }, [chosenDate]);
  const loadingErrorMessage = 'Erreur lors du chargement des concerts';

  const render = () => {
    if (loadingError)
      return toast.error('Erreur lors du chargement des concerts');
    if (loadingConcerts === true) return <Loading />;
    if (mode === eModes.nextShows) return <DisplayNextShows />;
    return (
      <DisplayTodaysConcerts
        loadingError={loadingError}
        todaysConcerts={todaysConcerts}
        chosenDate={chosenDate}
        cardSize={cardSize}
      />
    );
  };

  return (
    <div className="App container">
      <MainDate chosenDate={chosenDate} history={history} />
      <DateShortcutButtons
        chosenDate={chosenDate}
        history={history}
        setToMode={(bool) => {
          setToMode(bool);
        }}
      />
      {/* <CardSlider setCardSize={setCardSize} style={{ margin: 'auto' }} /> */}
      {loadingError && (
        <span style={{ fontSize: '1.6vw' }}>
          {loadingErrorMessage}
          <br />
          {loadingError}
        </span>
      )}
      {<div>{render()}</div>}
      <Footer />
    </div>
  );
}

export default Home;
