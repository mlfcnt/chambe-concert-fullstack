import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { orange, bleuFonce, grisClair } from '../constants/couleurs';
import { Settings, DateTime } from 'luxon';
Settings.defaultLocale = 'fr';

export const NextShowsCard = ({ concert }) => {
  const {
    venue,
    artist,
    description,
    id,
    price,
    startDate,
    style,
    tumbnailPicture,
    urlDescription,
    urlToBuyTicket,
  } = concert;
  const [expanded, setExpanded] = React.useState(false);

  const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 1000,
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
  }));
  const classes = useStyles();

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const generateTitle = () => {
    return (
      <Typography style={{ color: bleuFonce, fontSize: '1.2rem' }}>
        {DateTime.fromISO(startDate).toLocaleString(DateTime.TIME_24_SIMPLE)}{' '}
        {
          <span>
            -{' '}
            <span style={{ color: 'black' }}>
              {' '}
              {artist?.name || artist || '?'}{' '}
            </span>{' '}
          </span>
        }
        {style && <Typography color="textSecondary"> {style} </Typography>}
      </Typography>
    );
  };

  const generateSubHeader = () => {
    return (
      <Typography style={{ color: bleuFonce, fontSize: '1.2rem' }}>
        {venue?.name || '?'}{' '}
        {price && <span style={{ color: 'grey' }}> {price} </span>}
      </Typography>
    );
  };

  return (
    <Card className={classes.root}>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <CardHeader title={generateTitle()} subheader={generateSubHeader()} />
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="plus d'infos"
        >
          <ExpandMoreIcon />
        </IconButton>
      </div>

      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>{description}</Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
};
