import { useFetch } from './helper/hooks/useFetch';
import moment from 'moment';

// url du server
const url =
  process.env.NODE_ENV === 'production'
    ? 'https://chambe-concerts.herokuapp.com'
    : 'http://localhost:5000';

// url de l'api
const rootUrl = `${url}/api/concerts`;
const scrapperUrl = `${url}/api/scrappers`;
const mailUrl = `${url}/send`;
const spotify = `${url}/spotify`;
export function useConcerts() {
  return useFetch(rootUrl);
}

let aborter = null;

export async function getConcertsFromBDDByDate(date) {
  try {
    const res = await fetch(`${rootUrl}/date`, {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({ date }),
    });
    const { success, concerts } = await res.json();
    if (!success) return success;
    return { success, concerts };
  } catch (error) {
    console.error(error.message);
  }
}

export async function getNexConcertsFromBDDByDateRange(toDate) {
  try {
    const res = await fetch(`${rootUrl}/range`, {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({ toDate }),
    });
    const { success, concerts } = await res.json();
    if (success === false) return success;
    return { success, concerts };
  } catch (error) {
    console.error(error.message);
  }
}

export const getInfoConcerts = async (date = moment()) => {
  const formattedDate = moment(date).format('YYYY-MM-DD');
  const res = await fetch(`${scrapperUrl}/infoConcert`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({ date: formattedDate }),
  });
  const { success, concerts } = await res.json();
  if (!success) return success;
  return { success, concerts };
};

export const getConcertsJazzClub = async (date) => {
  const formattedDate = moment(date).format('YYYY-MM-DD');
  const res = await fetch(`${scrapperUrl}/jazzClub`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({ date: formattedDate }),
  });
  const { success, concerts } = await res.json();
  if (!success) return success;
  return { success, concerts };
};

export const getConcertsBrinDeZinc = async (date) => {
  const formattedDate = moment(date).format('YYYY-MM-DD');
  const res = await fetch(`${scrapperUrl}/brinDeZinc`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({ date: formattedDate }),
  });
  const { success, concerts } = await res.json();
  if (!success) return success;
  return { success, concerts };
};

export const sendEmail = async ({ name, email, subject, message }) => {
  const res = await fetch(`${mailUrl}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({ name, email, subject, message }),
  });
  const msg = await res.json();
  return msg;
};

export const getSpotifBestTrackForArtist = async (artistName) => {
  const res = await fetch(`${spotify}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({ artist: artistName }),
  });
  const { success, artist } = await res.json();
  if (!success || !artist) return false;
  return artist;
};
