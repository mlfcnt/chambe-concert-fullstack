import React from 'react';
import Home from './components/Home';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { BrowserRouter as Router } from 'react-router-dom';

function App() {
  return (
    <Router>
      <Home />
      <ToastContainer position={'top-right'} hideProgressBar autoClose={5000} />
    </Router>
    
  );
}

export default App;
