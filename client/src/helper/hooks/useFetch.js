import { useReducer, useEffect, useCallback } from 'react';

const responseTypes = {
  SUCCESS: 'SUCCESS',
  FAILURE: 'FAILURE'
};

function reducer(state, action) {
  switch (action.type) {
    case responseTypes.SUCCESS:
      return { loading: false, error: null, data: action.payload.data };
    case responseTypes.FAILURE:
      return { loading: false, error: action.payload.error, data: state.data };
    default:
      throw new Error(`Unknown useFetch response type ${action.type}`);
  }
}

export function useFetch(url, options = { verb: 'GET', useBearerToken: true }) {
  const { verb } = options;
  const [{ loading, error, data }, dispatch] = useReducer(reducer, {
    loading: true,
    error: null,
    data: null
  });
  const headers = options.useBearerToken
    ? {
        'Content-Type': 'application/json'
      }
    : {
        'Content-Type': 'application/json'
      };

  const run = useCallback(async () => {
    try {
      const res = await fetch(url, {
        method: verb,
        headers
      });

      if (res.status < 200 || res.status >= 300) {
        return dispatch({
          type: responseTypes.FAILURE,
          payload: { error: new Error(await res.text()) }
        });
      }

      const data = await res.json();
      dispatch({ type: responseTypes.SUCCESS, payload: { data } });
    } catch (error) {
      dispatch({ type: responseTypes.FAILURE, payload: { error } });
    }
  }, [url, headers, verb]);

  useEffect(() => {
    run();
  }, [url, run]);

  return { loading, error, data, refetch: run };
}
