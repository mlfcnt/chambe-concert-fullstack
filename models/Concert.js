const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');

const now = moment();

// Create Schema

const ConcertSchema = new Schema({
  startDate: {
    type: Date,
    default: now,
  },
  artist: {
    name: {
      type: String,
      required: true,
    },
    spotifyTopTrackUri: String,
  },
  description: String,
  price: String,
  style: String,
  urlToBuyTicket: String,
  urlDescription: String,
  largePicture: {
    type: String,
  },
  tumbnailPicture: String,
  defaultPicture: {
    type: String,
    default:
      'https://images.unsplash.com/photo-1524368535928-5b5e00ddc76b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
  },
  isPartOfFestival: Boolean,
  festivalName: String,
  venue: {
    name: {
      type: String,
      required: true,
    },
    twitterHandle: String,
  },
  isCanceled: {
    type: Boolean,
    default: false,
  },
  createdManually: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
  deletedAt: {
    type: Date,
    default: undefined,
  },
});

const Concert = mongoose.model('concert', ConcertSchema);
module.exports = Concert;
