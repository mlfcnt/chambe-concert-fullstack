const moment = require('moment');
const Concert = require('../models/Concert');

exports.getArtistTwitterHandle = async (client, concert) => {
  const artistName = concert.artist.name;
  const res = await client.get('users/search', {
    q: artistName,
    page: 1,
    count: 1,
  });
  if (!res[0]) return;
  const { name, screen_name, description, followers_count } = res[0];
  if (followers_count < 500) return;
  if (
    name.includes(artistName) ||
    screen_name.includes(artistName) ||
    description.includes(artistName)
  )
    return '@' + res[0].screen_name;
};

exports.getTodaysConcerts = async () => {
  const date = new Date();
  return Concert.find({
    startDate: {
      $gte: moment(date).startOf('day').toDate(),
      $lt: moment(date).endOf('day').toDate(),
    },
  }).sort({ date: 1 });
};
