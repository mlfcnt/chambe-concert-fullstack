const SpotifyWebApi = require('spotify-web-api-node');
require('dotenv').config();

// The API object we'll use to interact with the API
const spotifyApi = new SpotifyWebApi({
  clientId: process.env.SPOT_CLIENT_ID,
  clientSecret: process.env.SPOT_CLIENT_SECRET,
});

const createCredential = () => {
  // Using the Client Credentials auth flow, authenticate our app
  spotifyApi.clientCredentialsGrant().then(
    function (data) {
      // Save the access token so that it's used in future calls
      spotifyApi.setAccessToken(data.body['access_token']);
    },
    function (err) {
      console.log(
        'Something went wrong when retrieving an access token',
        err.message
      );
    }
  );
};
createCredential();
setInterval(function () {
  console.log('...creating spotify credentials...');
  spotifyApi.resetAccessToken();
  createCredential();
}, 1 * 60 * 60 * 1000); // 1 hour

const checkForArtistIndex = (artists, artistToFind) => {
  for (const artist of artists) {
    if (artist === artistToFind) return artists.indexOf(artistToFind);
  }
  return -1;
};

exports.getArtistTopTrackUri = async (artistName) => {
  let formattedArtistName = artistName
    .split('(')
    .join(',')
    .split('&')
    .join(',')
    .split('-')
    .join(',')
    .split('+')
    .join(',')
    .split('/')
    .join(',')
    .split(',')[0]
    .trim();

  try {
    const {
      body: { artists },
    } = await spotifyApi.searchArtists(`artist:${formattedArtistName}`, {
      limit: 10,
    });
    const artistNames = artists.items.map(({ name }) => name.toLowerCase());
    const goodArtistIndex = checkForArtistIndex(
      artistNames,
      formattedArtistName.toLowerCase()
    );

    if (goodArtistIndex === -1) return null;

    if (artists.items[goodArtistIndex]) {
      const topTracks = await spotifyApi.getArtistTopTracks(
        artists.items[0].id,
        'FR'
      );
      if (topTracks.body.tracks[0]) return topTracks.body.tracks[0].uri;
      return null;
    }
    return null;
  } catch (error) {
    console.log(`error while getting ${artistName} top track uri`);
    console.log(error);
  }
};
