const axios = require('axios');
const cheerio = require('cheerio');
const url = 'http://www.brindezinc.fr/spip.php?rubrique2';
const { v4: uuidv4 } = require('uuid');
const Moment = require('moment-timezone');
const Concert = require('../models/Concert');
var { Settings, DateTime } = require('luxon');
const { getArtistTopTrackUri } = require('./spotifyService');
const signale = require('signale');

Settings.defaultLocale = 'fr';
const saveBrinDeZinkConcerts = async () => {
  const html = await axios.get(url);
  const $ = cheerio.load(html.data);
  // le format des dates sur le site du brin de zink est vraiment pas pratique et non reconnu par Moment, j'ai donc créé un objet pour passer d'un mois type avr en 04
  // on stocke les concerts qui correspondent a la date ici
  const concerts = [];
  // on stocke les urls de la page bonus qui comprend la grande image
  const urlsLargePicture = [];
  // on stocke les urls de la page bonus qui comprend la description ici
  const urlsDescription = [];
  const artists = [];

  $('table[width="783"]').each(async (index, element) => {
    const rawConcertDate = getRawConcertDate($(element)); // ex : ' 13 déc'
    const startHour = getConcertTime($(element));

    const id = uuidv4();
    const artist = getArtist($(element), artists);
    const style = getStyle($(element));
    const price = getPrice($(element));
    getLargePicture($(element), urlsLargePicture, id);
    const tumbnailPicture = getTumbnail($(element));
    const urlDescription = getUrlDescription($(element), urlsDescription, id);

    concerts.push({
      id,
      startDate: getConcertDate(rawConcertDate, startHour).date,
      artist: {
        name: artist,
      },
      style,
      price,
      urlToBuyTicket: url,
      urlDescription,
      tumbnailPicture,
      venue: {
        name: 'Brin de Zinc',
        twitterHandle: '@BrinDeZinc',
      },
    });
  });
  const getDescription = async () => {
    for (const urlD of urlsDescription) {
      const { id, url } = urlD;
      const concertToEdit = concerts.find((c) => c.id === id);
      const html = await axios.get(url);
      const $ = cheerio.load(html.data);
      const style = $('#col2texte > span.texte > p').first().text();
      const description = $('#col2texte > span.texte')
        .first()
        .text()
        .replace(style, '');
      concertToEdit['description'] = description;
    }
  };
  await getDescription();

  const getImages = async () => {
    for (const urlLP of urlsLargePicture) {
      const { id, url } = urlLP;
      const concertToEdit = concerts.find((c) => c.id === id);
      const html = await axios.get(url);
      const $ = cheerio.load(html.data);
      const img = $('#draggable_cover_public').attr('src');
      img && (concertToEdit['largePicture'] = `https://www.billetweb.fr${img}`);
    }
  };

  const getUris = async () => {
    for (const artist of artists) {
      const uri = await getArtistTopTrackUri(artist);
      const concertToEdit = concerts.find(
        ({ artist: { name } }) => name === artist
      );
      concertToEdit.artist.spotifyTopTrackUri = uri;
    }
  };

  await getImages();
  await getUris();
  return concerts;
};

const brinDeZinkMonths = [
  'fakeMonthPourAvoirJanvierEnIndex1',
  'jan',
  'févr',
  'mars',
  'avr',
  'mai',
  'juin',
  'juil',
  'août',
  'sept',
  'oct',
  'nov',
  'déc',
];

const splitIfNeeded = (elem, caracterToCheck) => {
  if (elem.includes(caracterToCheck)) {
    return elem.split(caracterToCheck)[1];
  } else {
    return elem;
  }
};

const getYear = (month) => {
  if (month >= Moment().tz('Europe/Paris').format('MM'))
    return Moment().tz('Europe/Paris').format('YYYY');
  return Moment().tz('Europe/Paris').add(1, 'years').format('YYYY');
};

const getRawConcertDate = (elem) => elem.find('span.titre02rouge').text();

/**
 *
 * @param {*} rawConcertDate - La date récupérée par le crawler crawler - example : ' 23 oct. '
 */
const getConcertDate = (rawConcertDate, time) => {
  const tempformattedDate =
    rawConcertDate &&
    rawConcertDate.trim().slice(4, rawConcertDate.length).split('\n')[0];
  const hour = time.split('h')[0];
  const minute = time.split('h')[1];
  const day = tempformattedDate
    .split(' ')[1]
    .trim()
    .replace('er', '')
    .padStart(2, '0');
  const monthRaw = tempformattedDate.trim().split(' ')[1].replace('.', '');
  const month = String(brinDeZinkMonths.indexOf(monthRaw)).padStart(2, '0');
  const year = getYear(month);
  const date = DateTime.fromObject({
    year,
    month,
    day,
    hour,
    minute,
    zone: 'Europe/Paris',
  });
  return {
    day,
    monthRaw,
    month,
    year,
    date,
    time,
  };
};

const getConcertTime = (elem) => {
  return (
    elem
      .find('span.titre02rouge')
      .text()
      .trim()
      // ne récupère que les infos apres le || (l'heure)
      .split('||')[1]
      .trim()
      .replace(':', 'h')
  );
};

const getArtist = (elem, artists) => {
  const artist = elem
    .find('.titrearticle > a')
    .text()
    // le groupe est dans la même ligne que le style donc je fais un split
    .split('(')[0]
    .trimEnd();
  artists.push(artist);
  return artist;
};

const getStyle = (elem) => {
  return (
    splitIfNeeded(elem.find('.titrearticle > a').text(), '(')
      // enlève la parenthèse fermante
      .replace(')', '')
  );
};

const getPrice = (elem) => {
  let formattedPrice;
  const price = elem.find('.progaccueildate').text();
  if (price.length) {
    if (price.toLowerCase().includes('Prix libre'.toLowerCase()))
      return 'Prix libre';
    if (!price.includes('/')) return price;
    price.includes('€')
      ? (formattedPrice = price.split('/')[1].split('€')[0].trim() + '€')
      : (formattedPrice = price);
  } else {
    formattedPrice = null;
  }
  return formattedPrice;
};

const getLargePicture = (elem, urlsLargePicture, id) => {
  const urlLargePicture = elem.find('.texte .spip_url').html();
  if (urlLargePicture) {
    urlsLargePicture.push({ id, url: urlLargePicture });
  }
};

const getTumbnail = (elem) => {
  return `http://www.brindezinc.fr/${elem.find('.spip_logo').attr('src')}`;
};

const getUrlDescription = (elem, urlsDescription, id) => {
  const urlDescription = `http://www.brindezinc.fr/${elem
    .find('span.titre02rouge > a')
    .attr('href')}`;
  urlsDescription.push({
    id,
    url: urlDescription,
  });
  return urlDescription;
};

exports.saveBDZToBdd = async () => {
  console.log('Fetch des concerts du Brin de Zink en cours...');
  let count = 0;
  let concerts = [];
  try {
    concerts = await saveBrinDeZinkConcerts();
  } catch (error) {
    console.log('3333', error);
  }

  for (const concert of concerts) {
    const newConcert = new Concert(concert);
    const exists = await Concert.exists({
      startDate: newConcert.startDate,
      'artist.name': newConcert.artist.name,
    });

    if (!exists) {
      count = count + 1;
      newConcert
        .save()
        .then(() =>
          signale.info(`concert de ${concert.artist.name} enregistré! `)
        );
    } else {
      signale.info(
        `concert de ${concert.artist.name} NON enregistré car déjà présent `
      );
    }
  }
  signale.success('Fetch des concerts du Brin de Zink terminé !');
  signale.success(`${count} concerts du BDZ ajouté`);
  return count;
};
