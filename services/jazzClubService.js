const axios = require('axios');
const cheerio = require('cheerio');
const url = 'https://www.jazzclubdesavoie.fr/programme/';
const { v4: uuidv4 } = require('uuid');
const Moment = require('moment-timezone');
const Concert = require('../models/Concert');
const { Settings, DateTime } = require('luxon');
const signale = require('signale');

Settings.defaultLocale = 'fr';

exports.saveJazzClubConcerts = async () => {
  let concerts = [];
  try {
    const html = await axios.get(url);
    const $ = cheerio.load(html.data);

    $('.title').each(async (index, element) => {
      const startDate = getDate($(element));
      const artist = getArtist($(element));
      concerts.push({
        startDate,
        artist: {
          name: artist,
        },
        // style: 'Jazz',
        price: 'Adhésion requise',
        venue: {
          name: 'Jazz club de Savoie',
        },
      });
    });
    $('.align-middle').each(async (index, element) => {
      const description = getDescription($(element).text().trim());
      const isCanceled = getIsCanceled($(element).text().trim());
      concerts[index].description = description;
      concerts[index].isCanceled = isCanceled;
    });
    $('.img-fluid').each(async (index, element) => {
      const largePicture = getLargePicture($(element));
      concerts[index].largePicture = largePicture;
    });
  } catch (error) {
    console.log(error);
  }
  return concerts;
};

const getDate = (elem) => {
  const rawDate = elem.children().first().text();
  const onlyNumbers = rawDate.replace(/-/g, '');
  const day = onlyNumbers.slice(0, 2);
  const month = onlyNumbers.slice(2, 4);
  const year = onlyNumbers.slice(4, 8);
  return DateTime.fromObject({
    day,
    month,
    year,
    hour: 21,
    zone: 'Europe/Paris',
  });
};

const getArtist = (elem) => elem.children().last().text();

const getDescription = (elem) => {
  if (elem.toLowerCase() === 'concert annulé') return;
  return elem;
};

const getIsCanceled = (elem) => elem.toLowerCase() === 'concert annulé';

const getLargePicture = (elem) => {
  const url = elem.attr('src');
  return `https://www.jazzclubdesavoie.fr/${url}`;
};

exports.saveJCToBdd = async () => {
  console.log('Fetch des concerts du Jazz Club en cours...');
  let concerts = [];
  try {
    concerts = await saveJazzClubConcerts();
  } catch (error) {
    console.log('6666', error);
  }
  let count = 0;
  for (const concert of concerts) {
    const newConcert = new Concert(concert);
    const exists = await Concert.exists({
      startDate: newConcert.startDate,
      ' artist.name': newConcert.artist.name,
    });
    if (!exists) {
      newConcert
        .save()
        .then(() =>
          signale.info(`concert de ${concert.artist.name} enregistré! `)
        );
      count = count + 1;
    } else {
      signale.info(
        `concert de ${concert.artist.name} NON enregistré car déjà présent `
      );
    }
  }
  signale.success('Fetch des concerts du Jazz Club terminé !');
  console.success(`${count} concerts du Jazz Club ajoutés`);

  return count;
};
