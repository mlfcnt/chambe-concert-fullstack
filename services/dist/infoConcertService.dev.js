"use strict";

var axios = require('axios');

var cheerio = require('cheerio');

var url = 'https://www.infoconcert.com/ville/chambery-1386/concerts.html';

var _require = require('uuid'),
    uuidv4 = _require.v4;

var Moment = require('moment-timezone');

var MomentRange = require('moment-range');

var _require2 = require('../helpers/capitalizeHelper'),
    capitalizeFirstLetter = _require2.capitalizeFirstLetter;

var moment = MomentRange.extendMoment(Moment);

exports.getConcerts = function _callee(date, toDate) {
  var datesRange, formattedDate, isCanceled, urlsDescription, urlsLargePicture, html, $, cards, shouldReturn, getDescription, getImages;
  return regeneratorRuntime.async(function _callee$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          datesRange = moment.range(date, toDate);
          formattedDate = moment(date).format('YYYY-MM-DD');
          isCanceled = false;
          urlsDescription = [];
          urlsLargePicture = [];
          _context5.prev = 5;
          _context5.next = 8;
          return regeneratorRuntime.awrap(axios.get(url));

        case 8:
          html = _context5.sent;
          $ = cheerio.load(html.data);
          cards = [];

          shouldReturn = function shouldReturn(concertDate, formattedDate) {
            if (toDate) {
              if (moment(concertDate).within(datesRange)) {
                return true;
              }

              return false;
            }

            if (concertDate.includes(formattedDate)) {
              return true;
            }
          }; // On choppe les cartes qui ont pour startdate la date d'aujourd'hui


          $('.panel-body').each(function (index, element) {
            $(element).find('div.row').find('div.date').find('time').filter(function () {
              var concertDate = $(this).attr('datetime');

              if (shouldReturn(concertDate, formattedDate)) {
                if ($(element).find('span.btn_annul').text().length > 0) {
                  isCanceled = true;
                }

                var id = uuidv4();
                var startDate = moment($(this).attr('datetime')).tz('Europe/Paris').format();
                var artist = $(element).find('div.row').find('div.summary').find('a').html();
                var venue = $(element).find('div.row').find('div.summary').find('div.location').find('span').html().replace('A Chambery', '').trim();
                var price = $(element).find('div.price').text().trim();
                var urlDescription = 'https://www.infoconcert.com/' + $(element).find('div.spectacle > a').attr('href');
                urlsDescription.push({
                  id: id,
                  url: urlDescription
                });

                var _url = $(element).find('a:contains("RÉSERVEZ VITE !")').attr('href');

                if (_url) {
                  var formattedUrl = (_url.includes('digitick') ? '' : 'https://www.infoconcert.com') + $(element).find('a:contains("RÉSERVEZ VITE !")').attr('href');
                  urlsLargePicture.push({
                    id: id,
                    url: formattedUrl
                  });
                }

                cards.push({
                  id: id,
                  startDate: startDate,
                  artist: capitalizeFirstLetter(artist),
                  venue: venue,
                  isCanceled: isCanceled,
                  price: price,
                  urlDescription: urlDescription
                });
              }
            });
          });

          getDescription = function getDescription() {
            var _iteratorNormalCompletion, _didIteratorError, _iteratorError, _loop, _iterator, _step;

            return regeneratorRuntime.async(function getDescription$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _iteratorNormalCompletion = true;
                    _didIteratorError = false;
                    _iteratorError = undefined;
                    _context2.prev = 3;

                    _loop = function _loop() {
                      var urlD, id, url, concertToEdit, html, $, description, _description;

                      return regeneratorRuntime.async(function _loop$(_context) {
                        while (1) {
                          switch (_context.prev = _context.next) {
                            case 0:
                              urlD = _step.value;
                              id = urlD.id, url = urlD.url;
                              concertToEdit = cards.find(function (c) {
                                return c.id === id;
                              });
                              _context.next = 5;
                              return regeneratorRuntime.awrap(axios.get(url));

                            case 5:
                              html = _context.sent;
                              $ = cheerio.load(html.data);

                              if ($('p.resume')) {
                                description = $('p.resume').text();
                                concertToEdit['description'] = description;
                              } else {
                                _description = $('div[style="text-align: justify;"]').text();
                                concertToEdit['description'] = _description;
                              } // const image = $('img').html();


                            case 8:
                            case "end":
                              return _context.stop();
                          }
                        }
                      });
                    };

                    _iterator = urlsDescription[Symbol.iterator]();

                  case 6:
                    if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                      _context2.next = 12;
                      break;
                    }

                    _context2.next = 9;
                    return regeneratorRuntime.awrap(_loop());

                  case 9:
                    _iteratorNormalCompletion = true;
                    _context2.next = 6;
                    break;

                  case 12:
                    _context2.next = 18;
                    break;

                  case 14:
                    _context2.prev = 14;
                    _context2.t0 = _context2["catch"](3);
                    _didIteratorError = true;
                    _iteratorError = _context2.t0;

                  case 18:
                    _context2.prev = 18;
                    _context2.prev = 19;

                    if (!_iteratorNormalCompletion && _iterator["return"] != null) {
                      _iterator["return"]();
                    }

                  case 21:
                    _context2.prev = 21;

                    if (!_didIteratorError) {
                      _context2.next = 24;
                      break;
                    }

                    throw _iteratorError;

                  case 24:
                    return _context2.finish(21);

                  case 25:
                    return _context2.finish(18);

                  case 26:
                  case "end":
                    return _context2.stop();
                }
              }
            }, null, null, [[3, 14, 18, 26], [19,, 21, 25]]);
          };

          _context5.next = 16;
          return regeneratorRuntime.awrap(getDescription());

        case 16:
          if (urlsLargePicture) {
            _context5.next = 20;
            break;
          }

          return _context5.abrupt("return", cards);

        case 20:
          getImages = function getImages() {
            var _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _loop2, _iterator2, _step2;

            return regeneratorRuntime.async(function getImages$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _iteratorNormalCompletion2 = true;
                    _didIteratorError2 = false;
                    _iteratorError2 = undefined;
                    _context4.prev = 3;

                    _loop2 = function _loop2() {
                      var urlLP, id, url, concertToEdit, html, $, img, _img;

                      return regeneratorRuntime.async(function _loop2$(_context3) {
                        while (1) {
                          switch (_context3.prev = _context3.next) {
                            case 0:
                              urlLP = _step2.value;
                              id = urlLP.id, url = urlLP.url;
                              concertToEdit = cards.find(function (c) {
                                return c.id === id;
                              });
                              _context3.next = 5;
                              return regeneratorRuntime.awrap(axios.get(url));

                            case 5:
                              html = _context3.sent;
                              $ = cheerio.load(html.data);

                              if (url.includes('digitick')) {
                                img = $('img.block-image').attr('src');
                                concertToEdit['largePicture'] = img;
                              } else {
                                _img = $('figure.figure > img').attr('src');
                                concertToEdit['largePicture'] = 'https://infoconcert.francebillet.com/' + _img;
                              }

                            case 8:
                            case "end":
                              return _context3.stop();
                          }
                        }
                      });
                    };

                    _iterator2 = urlsLargePicture[Symbol.iterator]();

                  case 6:
                    if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                      _context4.next = 12;
                      break;
                    }

                    _context4.next = 9;
                    return regeneratorRuntime.awrap(_loop2());

                  case 9:
                    _iteratorNormalCompletion2 = true;
                    _context4.next = 6;
                    break;

                  case 12:
                    _context4.next = 18;
                    break;

                  case 14:
                    _context4.prev = 14;
                    _context4.t0 = _context4["catch"](3);
                    _didIteratorError2 = true;
                    _iteratorError2 = _context4.t0;

                  case 18:
                    _context4.prev = 18;
                    _context4.prev = 19;

                    if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
                      _iterator2["return"]();
                    }

                  case 21:
                    _context4.prev = 21;

                    if (!_didIteratorError2) {
                      _context4.next = 24;
                      break;
                    }

                    throw _iteratorError2;

                  case 24:
                    return _context4.finish(21);

                  case 25:
                    return _context4.finish(18);

                  case 26:
                  case "end":
                    return _context4.stop();
                }
              }
            }, null, null, [[3, 14, 18, 26], [19,, 21, 25]]);
          };

          _context5.next = 23;
          return regeneratorRuntime.awrap(getImages());

        case 23:
          return _context5.abrupt("return", cards);

        case 24:
          _context5.next = 30;
          break;

        case 26:
          _context5.prev = 26;
          _context5.t0 = _context5["catch"](5);
          console.error({
            error: _context5.t0
          });
          throw new Error(_context5.t0);

        case 30:
        case "end":
          return _context5.stop();
      }
    }
  }, null, null, [[5, 26]]);
};