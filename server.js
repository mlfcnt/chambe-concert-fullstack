const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const path = require('path');
const concerts = require('./routes/api/concerts');
const mail = require('./routes/sendMail');
const misc = require('./routes/api/misc');
const {
  saveConcertsEveryHour,
  tweetApresMidi,
  buildAndDeploy,
} = require('./crons');

const app = express();
require('dotenv').config();
// Middleware qui permet d'autoriser les requêtes CORS entre nos client et le server
app.use(cors());
// Middleware qui permet de parser le body de nos POST requêtes
app.use(express.json());

//Connexion à la BDD
mongoose
  .connect(process.env.MONGODB_URI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
  })
  .then(() => console.log('MongoDB ✔️'))
  .catch((err) => console.error(err));

// Middleware qui renvoi les requêtes de /api/concerts vers notre fichier ./routes/api/concerts
app.use('/api/concerts', concerts);
app.use('/send', mail);
app.use('/api/misc', misc);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server ✔️ Port ${port}`));

if (process.env.NODE_ENV === 'production') {
  //En prod (Heroku), le front end est le fichier JS buildé
  app.use(express.static('client/build'));
  // Au lieu d'une erreur 404, renvoyer vers index.html
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
  //CRONS;
  (async () => {
    await saveConcertsEveryHour(); // crawl les sources et rempli la BDD si besoin toutes les heures
    // await tweetApresMidi(); // tweet les concerts du jour à 15:30
    await buildAndDeploy(); // build et deploy le client sur vercel
  })();
}
