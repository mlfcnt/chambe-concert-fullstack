/**
 *
 * @param {string} string String à formatter
 * @param {string} [type] Si le type est 'sentence', capitalize la première lettre du premier mot uniquement.
 */
exports.capitalizeFirstLetter = (string, type) => {
  if (type === 'sentence') {
    const stringtoUppercase = string.split(' ')[0];
    return (
      stringtoUppercase.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
    );
  }
  const newString = string.toLowerCase();
  return newString.charAt(0).toUpperCase() + newString.slice(1);
};

/**
 * Capitalize la premiere lettre de chaque mots de la phrase
 * @param {*} sentence
 */
exports.capitalizeFirstLetters = (sentence) => {
  return sentence
    .toLowerCase()
    .split(' ')
    .map((word) => word.charAt(0).toUpperCase() + word.substring(1))
    .join(' ');
};
